.PHONY: 
vpath %.c src
vpath %.h include
OPATH=obj



#NAME ?= $(shell basename $(shell pwd))
LDLIBS ?= -lm -lncursesw
CFLAGS ?= -O3 -march=native -g3 -ggdb


override CFLAGS += -std=gnu11 -pedantic -pedantic-errors \
		-Wall -Wextra \
		-Wdouble-promotion -Wformat=2 -Winit-self -Wswitch-default \
		-Wswitch-enum -Wunused-parameter -Wuninitialized -Wfloat-equal \
		-Wshadow -Wundef -Wbad-function-cast -Wcast-qual -Wcast-align \
		-Wwrite-strings -Wconversion -Wstrict-prototypes \
		-Wold-style-definition -Wmissing-prototypes -Wmissing-declarations \
		-Wredundant-decls -Wnested-externs
# D'autres warnings intéressants (en général, certains sont inutiles dans ce
# cas particulier) mais pas encore reconnus par la version de GCC disponible
# sur une Ubuntu 14.04... :
#
# -Wmissing-include-dirs -Wnull-dereference -Wswitch-bool -Wduplicated-cond
# -Wdate-time

#EXEC = spaceship-infinity
#all: $(EXEC)
spaceship-infinity: spaceship-infinity.o options.o game.o column_list.o terrain.o ui.o column.o point_list.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LOADLIBES) $(LDLIBS)

#	$(CC) $(LDFLAGS) $^ -o $@ $(LOADLIBES) $(LDLIBS)
#	    $(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $< $(LOADLIBES) $(LDLIBS)

# Archive
dist:
	tar -czf $(NAME).tar.gz --transform="s,^,$(NAME)/," *.c *.h Makefile

# Nettoyage

bin/spaceship-infinity : $(OPATH)/spaceship-infinity.o $(OPATH)/options.o $(OPATH)/game.o $(OPATH)/column_list.o $(OPATH)/terrain.o $(OPATH)/ui.o $(OPATH)/column.o $(OPATH)/point_list.o
# 	$(CC) $(CFLAGS) -o $@ $^ -L -ljeu
	$(CC) $(LDFLAGS) $^ -o $@ $(LOADLIBES) $(LDLIBS)


#	$(CC) $(LDFLAGS) $(LOADLIBES) $(LDLIBS) $< -o $@

$(OPATH)/spaceship-infinity.o : spaceship-infinity.c game.h point.h point_list.h terrain.h column.h cell.h column_list.h options.h ui.h version.h
$(OPATH)/ui.o : ui.c ui.h game.h point.h point_list.h terrain.h column.h cell.h column_list.h options.h
$(OPATH)/game.o : game.c game.h point.h point_list.h terrain.h column.h cell.h column_list.h options.h
$(OPATH)/terrain.o : terrain.c terrain.h point.h column.h cell.h column_list.h
$(OPATH)/column_list.o : column_list.c column_list.h column.h cell.h
$(OPATH)/column.o : column.c column.h cell.h
$(OPATH)/options.o  : options.c options.h
$(OPATH)/point_list.o : point_list.c point_list.h point.h

# $(OPATH)/%.o : %.c | $(OPATH)
# 	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@
# 	$(CC) $(CFLAGS) $(LOADLIBES) $(LDLIBS) -c $< -o $@


# 	    $(CC) $(CFLAGS) $(CPPFLAGS) $(LOADLIBES) $(LDLIBS) -c -o $@ $<
# 	$(CC) $(LDFLAGS) -c $< -o $@ $(LOADLIBES) $(LDLIBS)

#	$(CC) $(LDFLAGS) $^ -o $@ $(LOADLIBES) $(LDLIBS)

#lib/lib_game.a : $(OPATH)/spaceship-infinity.o $(OPATH)/ui.o $(OPATH)/terrain.o $(OPATH)/column_list.o $(OPATH)/column.o  $(OPATH)/point_list.o  $(OPATH)/options.o
#	ar rcs lib/lib_game.a $(OPATH)/spaceship-infinity.o $(OPATH)/ui.o $(OPATH)/terrain.o $(OPATH)/column_list.o $(OPATH)/column.o  $(OPATH)/point_list.o  $(OPATH)/options.o


# Dépendances avec les en-têtes

clean:
	-rm -rf *.o spaceship-infinity $(OPATH)/spaceship-infinity.o $(OPATH)/ui.o $(OPATH)/terrain.o $(OPATH)/column_list.o $(OPATH)/column.o  $(OPATH)/point_list.o  $(OPATH)/options.o

distclean: clean
	$(RM) *.tar.gz

