/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include "../include/point_list.h"

#include <stdlib.h>

/* If you need other headers, include them here: */

////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////

/* Add other structures as you see fit: */

struct plist{
	point point;
	struct plist* next;
	struct plist* prev;
};

struct point_list
{
	plist* body;
	uint size;
};

////////////////////////////////////////////////////////////////////////////////
// local functions declarations
////////////////////////////////////////////////////////////////////////////////

static plist* build();
static plist *get_list_tail(plist *pBody);
static plist *get_list_front(plist *pBody);
static plist *get_list_next(plist *pBody);
static plist *get_list_prev(plist *pBody);
static inline bool isInsight(point point,struct point up_left, struct point bottom_right);
static void destroy_point(point_list *const pList, struct plist *pPlist);


////////////////////////////////////////////////////////////////////////////////
// init./destroy etc.
////////////////////////////////////////////////////////////////////////////////



point_list* point_list_new(void)
{
	point_list* pointList = (point_list*)malloc(sizeof(struct point_list));
	if (NULL == pointList)
		exit(-1);
	pointList->size = 0;
	pointList->body = NULL;
	return pointList;
}

void point_list_destroy(point_list* const l)
{
	if(NULL == l)
		return;
	plist* it = l->body;
	plist* rl;
	while (NULL != it){
		rl = it;
		it = it->next;
		free(rl);
	}
	free(l);
}


////////////////////////////////////////////////////////////////////////////////
// getters
////////////////////////////////////////////////////////////////////////////////

point point_list_get_point(const point_list* const l, size_t i)
{
	if(i > l->size)
		return point_invalid();
	plist* itPoint = l->body;
	for (int j = 0; j < i; ++j) {
		itPoint = get_list_next(itPoint);
	}
	return itPoint->point;
}



size_t point_list_get_size(const point_list* const l)
{
	return l->size;
}

bool point_list_contains(const point_list* const l, point p)
{
	
	plist* itPlist = l->body;
	while (itPlist != NULL){
		if (point_equals(itPlist->point,p))
			return true;
		itPlist = itPlist->next;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

point_list* point_list_push_front(point_list* const l, point p)
{
	plist* data = build();
	data->point = p;
	if (NULL == l)
		return NULL;

	plist* list = get_list_front(l->body);
	if (NULL == list){
		l->body = data;
		l->size++;
		return l;
	}
	list->prev = data;
	data->next = list;
	l->body = data;
	l->size++;
	return l;
}


point_list* point_list_push_back(point_list* const l, point p)
{
	plist* data = build();
	data->point = p;

	plist* last = get_list_tail(l->body);
	if (NULL == last){
		l->body = data;
		l->size++;
		return l;
	}
	last->next = data;
	data->prev = last;
	l->size++;

	return l;
}

point_list* point_list_pop_front(point_list* const l)
{
	if (NULL == l)
		return NULL;

	if (l->body == NULL)
		return l;

	plist* front = l->body;
	plist* next = get_list_next(front);
	l->body = next;
	l->body->prev = NULL;
	free(front);
	l->size--;

	return l;	return NULL;
}

point_list* point_list_pop_back(point_list* const l)
{
	if (NULL == l)
		return NULL;

	if (l->body == NULL)
		return l;
	plist* last = get_list_tail(l->body);
	if (NULL == get_list_prev(last))
		l->body = NULL;
	if (last->prev)
		last->prev = NULL;

	free(last);
	l->size--;
	return l;
}

void point_list_set_point(point_list* const l, size_t i, point p)
{
	if (l->size >i){
		plist* itplist = l->body;
		for (int j = 0; j < i; ++j) {
			itplist=get_list_next(itplist);
		}
		itplist->point=p;
	}
}


point_list* point_list_prune_out_of_bounds(
		point_list* const l, point up_left, point bottom_right)
{
	if (NULL == l) return l;
	if (NULL == l->body) return l;
	struct plist* points = l->body;
	while (NULL != points) {
		if (!isInsight(points->point,up_left,bottom_right)) {
		destroy_point(l,points);
		}
		points = points->next;
	}
	return l;
}



void point_list_shift_left(point_list* const l)
{
	struct plist* points = l->body;
	while (NULL != points){
		points->point.x--;
		points = points->next;
	}
}
bool cab_be_shifted(plist* p){
	switch (p->point.gun){

		case GUN_BLASTER:
			return true;
		case GUN_LASER:return false;
		case GUN_ROCKET:return true;
		case GUN_DIE_MOTHER_FUCKER_DIE:return true;
	}
}

void point_list_shift_right(point_list* const l)
{
	struct plist* points = l->body;
	while (NULL != points) {
		if (cab_be_shifted(points))
			points->point.x++;
		points = points->next;
	}
}

////////////////////////////////////////////////////////////////////////////////
// local functions definitions
////////////////////////////////////////////////////////////////////////////////

plist *get_next_point(plist *pPlist) {
	return pPlist->next;
}

static plist *build() {
	plist* newEll = (plist*)malloc(sizeof(struct plist));
	if(NULL == newEll)
		return NULL;
	newEll->next = NULL;
	newEll->prev = NULL;
	return newEll;
}

static plist *get_list_front(plist *pBody) {
	plist* it = pBody;
	if (NULL == it)
		return NULL;
	while (it->prev != NULL){
		it = it->prev;
	}
	return it;
}
static plist *get_list_tail(plist *pBody) {
	plist* it = pBody;
	if (NULL == it)
		return NULL;
	while (it->next != NULL){
		it = it->next;
	}
	return it;
}



static plist *get_list_next(plist *pBody) {
	return (pBody)?pBody->next:NULL;
}

static plist *get_list_prev(plist *pBody) {
	return (pBody)?pBody->prev:NULL;
}

	static bool isInsight(point point,struct  point up_left,struct  point bottom_right){
		return  (
				(point.x >= up_left.x) &&
		    (point.x <= bottom_right.x) &&
		    (point.y >= up_left.y) &&
		    (point.y <= bottom_right.y)
		    );
	}
bool point_list_contains_on_y(point_list *l, point point) {
	plist* itPlist = l->body;
	while (itPlist != NULL){
		if (itPlist->point.y == point.y)
			return true;
		itPlist = itPlist->next;
	}
	return false;
}
void destroy_point(point_list *const pList, struct plist *pPlist) {
	struct plist* it = get_list_front(pList->body);
	struct plist* rit = NULL;
	if(NULL == pPlist) return;

	while (it != pPlist){
		it = it->next;
	}
	if (NULL == it)
		return;
	rit = it;

	if(it->next)
		rit->next->prev = it->prev;
	if(it->prev)
		it->prev->next  = it->next;
	it = get_list_front(it);
	if (it == rit)
		pList->body = get_list_next(it);
	pList->size--;
	free(rit);

}