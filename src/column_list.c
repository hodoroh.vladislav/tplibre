/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include "../include/column_list.h"
#include "../include/point.h"

#include <stdlib.h>

/* If you need other headers, include them here: */

////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////

/* Add other structures or delete them as you see fit: */
struct useless
{
	char lie;
	int trouvable;
};
typedef struct useless useless;

struct columnBody{
	column* column;
	struct columnBody* next;
	struct columnBody* prev;
};


//liste doublement chainne circulaire
struct column_list
{
	columnBody* body;
	size_t size;
};

////////////////////////////////////////////////////////////////////////////////
// local functions declarations
////////////////////////////////////////////////////////////////////////////////
columnBody* build();
columnBody *get_list_tail(columnBody *pBody);
columnBody *get_list_front(columnBody *pBody);
columnBody *get_list_next(columnBody *pBody);
columnBody *get_list_prev(columnBody *pBody);
column *get_list_column(columnBody *pBody);

/* If you need auxiliary functions, declare them here: */


////////////////////////////////////////////////////////////////////////////////
// init./destroy etc.
////////////////////////////////////////////////////////////////////////////////



column_list* column_list_new(void)
{
	column_list* list =  malloc(sizeof(struct column_list));
	if (NULL == list)
		exit(-1);
	list->size = 0;
	list->body = NULL;
	return list;
}





void column_list_destroy(column_list* const l)
{
	if (NULL != l){
		columnBody* it = l->body;
		columnBody* rl;
		while (NULL != it){
			rl = it;
			it = it->next;
			free(rl);
		}
		free(l);
	}
}

////////////////////////////////////////////////////////////////////////////////
// getters
////////////////////////////////////////////////////////////////////////////////

column* column_list_get_column(const column_list* const l, const size_t i)
{
	if(!l || !l->body)
		return NULL;
	if (l->size >= 0)
		if (i < l->size){
			columnBody* list = (columnBody*) get_list_front(l->body);
			for (size_t j = 0; j <= i; ++j)
				if (NULL != list->next)
					list = list->next;
			return list->column;

		}
	return NULL;

}

size_t column_list_get_size(const column_list* const l)
{
	return l->size;
}

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

column_list* column_list_push_front(column_list* const l, column* const c)
{
	columnBody* data = build();
	data->column = c;
	if (NULL == l)
		return NULL;

	columnBody* list = get_list_front(l->body);
	if (NULL == list){
		l->body = data;
		l->size++;
		return l;
	}
	list->prev = data;
	data->next = list;
	l->body = data;
	l->size++;
	return l;
}



column_list* column_list_push_back(column_list* const l, column* const c)
{
	columnBody* data = build();
	data->column = c;

	columnBody* last = get_list_tail(l->body);
	if (NULL == last){
		l->body = data;
		l->size++;
		return l;
	}
	last->next = data;
	data->prev = last;
	l->size++;

	return l;
}

column_list* column_list_pop_front(column_list* const l)
{
	if (NULL == l)
		return NULL;

	if (l->body == NULL)
		return l;

	columnBody* front = get_list_front(l->body);
	columnBody* next = get_list_next(front);
	l->body = next;
	next->prev = NULL;
	free(front);
	l->size--;

	return l;
}

void column_list_set_row_empty(column_list* columnList, point ship){
	struct columnBody* listBody = column_list_get_from_index(columnList,ship.x);
	while (NULL != listBody){
		column_set_cell(listBody->column,ship.y,CELL_EMPTY);
		listBody = listBody->next;
	}
}
struct columnBody* column_list_get_from_index(struct column_list* columnList, size_t index){
	struct columnBody* it = columnList->body;

	if(NULL == it)
		return NULL;

	for (int i = 0; i < index; ++i) {
		if (NULL != it)
			it = it->next;
		else{
			it = NULL;
			break;
		}
	}
	return it;
}
column_list* column_list_pop_back(column_list* const l)
{
	if (NULL == l)
		return NULL;

	if (l->body == NULL)
		return l;

	columnBody* last = get_list_tail(l->body);
	if (NULL == get_list_prev(last))
		l->body = NULL;
	if (last->prev)
		last->prev->next = NULL;

	free(last);
	l->size--;
	return l;
}



////////////////////////////////////////////////////////////////////////////////
// local functions definitions
////////////////////////////////////////////////////////////////////////////////
columnBody* build(){
	columnBody* list = (columnBody*)malloc(sizeof(struct columnBody));
	if(NULL == list)
		return NULL;
	list->next = NULL;
	list->prev = NULL;
	return list;
}

columnBody *get_list_front(columnBody *pBody) {
	columnBody* it = pBody;

	if (NULL == it)
		return NULL;

	while (it->prev != NULL){
		it = it->prev;
	}
	return it;
}
columnBody *get_list_tail(columnBody *pBody) {
	columnBody* it = pBody;
	if (NULL == it)
		return NULL;

	while (it->next != NULL){
		it = get_list_next(it);
	}

	return it;
}

columnBody *get_list_next(columnBody *pBody) {
	return pBody->next;
}

columnBody *get_list_prev(columnBody *pBody) {
	return pBody->prev;
}
column *get_list_column(columnBody *pBody) {
	return pBody->column;
}




/* If you need auxiliary functions, define them here: */


void column_list_set_cell_on_radius(column_list* columnList, point position, enum cell cell,ushort radius){


//	column_set_cell_from_to(col,position.y-radius,position.y+radius,cell);
	size_t toI = ((position.x+radius) <= column_list_get_size(columnList))?(position.x+radius):column_list_get_size(columnList);

	size_t rad = 0;


	for (size_t i =	((position.x-radius) >= 0)?(position.x-radius):0;i < toI ; ++i) {

		(i <= position.x)?rad++:rad--;
		column_set_cell_from_to(
				column_list_get_column(columnList,i),
				position.y-rad,
				position.y+rad,
				cell);

	}
}