/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include "../include/column.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <sysexits.h>

/* If you need other headers, include them here: */

////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////

struct column
{
	cell* cells;
	int height;
};

////////////////////////////////////////////////////////////////////////////////
// init./destroy etc.
////////////////////////////////////////////////////////////////////////////////

column* column_new(const int height, const int low, const int high)
{
	column* const c = malloc(sizeof *c);
	if (!c)
	{
		perror("malloc");
		exit(EX_OSERR);
	}
	cell* const cells = malloc(sizeof *cells * (size_t) height);
	if (!cells)
	{
		perror("malloc");
		exit(EX_OSERR);
	}

	for (int i = 0; i < height; ++i)
		cells[i] = i > high || i < low ? CELL_WALL : CELL_EMPTY;
	c->cells = cells;
	c->height = height;

	return c;
}

void column_destroy(column* const c)
{
	if (!c)
		return;

	if (c->cells)
		free(c->cells);
	free(c);
}

////////////////////////////////////////////////////////////////////////////////
// getters
////////////////////////////////////////////////////////////////////////////////

cell column_get_cell(const column* const c, const size_t i)
{
	return c && c->cells && i < (size_t) c->height ? c->cells[i] : CELL_EMPTY;
}

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

void column_set_cell(column* const c, const size_t i, const cell x)
{
	if (c)
		c->cells[i] = x;
}

void column_set_cell_from_to(column* const c, const short from, const short to, const cell x){

	size_t toI = (to < c->height && to > 0)? to : (to < c->height)?0:c->height;
	size_t fromI = (from < c->height && from > 0)?from:(from < c->height)?0:c->height;


	for (size_t i = fromI; i < toI; ++i) {
		column_set_cell(c,i,x);
	}
}

size_t getNextObject(cell cell,size_t index, column* const c){
	for (size_t i = index; i < c->height; ++i) {
		if (c->cells[i] == cell)
			return i;
	}
	return 0;
}

bool can_fall(column* const column){
	if (column->cells[0] == CELL_EMPTY && column->cells[column->height-1] == CELL_EMPTY)
		return false;
	for (int i = 0; i < column->height; ++i) {
		if (column->cells[i] == CELL_EMPTY){
			if(getNextObject(CELL_EMPTY,getNextObject(CELL_WALL,i,column),column))
				return true;
		}
	}

	return false;
}




void column_fall(column* const c)
{
	while(can_fall(c))
	{
		size_t nextObj = getNextObject(CELL_EMPTY,0,c);
		for (size_t i = c->height-1; i > nextObj; --i) {
			if (c->cells[i] == CELL_EMPTY && c->cells[i-1]!=CELL_EMPTY){
				c->cells[i] = c->cells[i-1];
				c->cells[i-1] = CELL_EMPTY;
			}
		}
		break;
	}
}
