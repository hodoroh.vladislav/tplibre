/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include <stdio.h>

#include <tgmath.h>
#include <float.h>
#include <sysexits.h>


#include "../include/game.h"

////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////


struct ship{
	point ship_pos;
	enum gun gun;
	short life;
};


struct game
{
	spaceship_options options;
	terrain* map;
	struct ship ship;
	bool debug;
	double delay;
	intmax_t bonus;
	int last_key;
	double fire_time;
	/* Misc. time variables. */
	double elapsed_time;
	double start_timestamp;
	double last_turn_timestamp;

	/* Bullet related fields. */
	size_t bullet_ammo;
	size_t max_ammo;
	point_list* bullets;
	double cooldown_start;
	double cooldown_threshold;
	short max_life;
	short max_authorised_life;
	short health;
	bool isFireing;

};







////////////////////////////////////////////////////////////////////////////////
// local functions declarations
////////////////////////////////////////////////////////////////////////////////

static intmax_t game_get_bonus(const game* g);
static column* game_get_ship_column(const game* g);

static void game_shift_right(game* g);
static void game_move_bullets(game* g);
static void game_fall(game* g);
static void game_check_bullets(game* g);
static void game_check_special_cells(game* g);
static void game_add_bonus(game* g, intmax_t bonus);

static bool game_turn_expected(game* g, double timestamp);
static double _threshold(const spaceship_options options, const double d);

////////////////////////////////////////////////////////////////////////////////
// init./destroy etc.
////////////////////////////////////////////////////////////////////////////////





game* game_init(const spaceship_options options)
{
	const int height = options.height;
	const int w = options.width;
	const int difficulty = options.difficulty;
	const int bullet_ammo = options.bullet_start_ammo;

//for manual (in code) gun change
//	const enum gun gun = GUN_BLASTER;
//	const enum gun gun = GUN_LASER;
//	const enum gun gun = GUN_ROCKET;
	const enum gun gun = GUN_DIE_MOTHER_FUCKER_DIE;


	game* const g = malloc(sizeof *g);
	if (!g)
	{
		perror("malloc");
		exit(EX_OSERR);
	}
	terrain* const map = terrain_init(height, w, difficulty);
	if (!map)
	{
		perror("malloc");
		exit(EX_OSERR);
	}
	/*
	 * Select an empty cell for the ship... we don't want the game to be over
	 * right away
	 */

	g->ship =  (struct ship) {
			.gun = gun,
			.life = 100,
			.ship_pos = terrain_start_point(map),
	};

	g->map = map;
	g->last_key = 0;
	g->debug = options.debug;
	g->options = options;
	g->bonus = 0;

	g->elapsed_time = 0.0;
	g->start_timestamp = 0.0;
	g->last_turn_timestamp = 0.0;
	g->delay = DBL_MIN;

	/* Bullets. */
	g->cooldown_start = 0.0;
	g->cooldown_threshold = options.bullet_cooldown;
	if (bullet_ammo > 0)
		g->max_ammo = (size_t) bullet_ammo;
	else
		g->max_ammo = difficulty < 3 ? 5 - (size_t) difficulty : 1;
	g->bullet_ammo = g->max_ammo;
	g->bullets = point_list_new();
	g->max_life = 100;
	g->max_authorised_life = 200;
	g->health = OPTIONS_DEFAULT_HEALTH;
	g->fire_time = 0.0;
	g->isFireing = false;
	return g;
}
void game_set_fireTime(game* g,double current){
	g->fire_time = current;
}

void game_destroy(game* const g)
{
	if (!g)
		return;

	if (g->map)
		terrain_destroy(g->map);
	if (g->bullets)
		point_list_destroy(g->bullets);
	free(g);
}

////////////////////////////////////////////////////////////////////////////////
// getters
////////////////////////////////////////////////////////////////////////////////

double game_get_delay(const game* const g)
{
	return g->delay;
}



double game_get_elapsed_time(const game* const g)
{
	return g->elapsed_time;
}
intmax_t game_get_score(const game* const g)
{
	const double elapsed = game_get_elapsed_time(g);
	const intmax_t bonus = game_get_bonus(g);
	const intmax_t score = bonus + (intmax_t) elapsed * 10;
	return score;
}

spaceship_options game_get_options(const game* const g)
{
	return g->options;
}

terrain* game_get_map(const game* const g)
{
	return g->map;
}

point game_get_ship_position(const game* const g)
{
	return g->ship.ship_pos;
}

size_t game_get_bullet_ammo(const game* const g)
{

	return g->bullet_ammo;
}

size_t game_get_bullet_max_ammo(const game* const g)
{
	return g->max_ammo;
}

point_list* game_get_bullets(const game* const g)
{
	return g->bullets;
}

int game_get_last_input(const game* const g)
{
	return g->last_key;
}

bool game_ship_is_alivee(const game* const g)
{
	const struct ship ship = g->ship;
	const column* const c = game_get_ship_column(g);
	const cell position = column_get_cell(c, (size_t) ship.ship_pos.y);
	return position != CELL_WALL;
}

void wallColision(game *pGame) {
	game_sub_life(pGame,10 + ((int) random() % 40));
}

bool game_ship_is_alivebck(const game* const g)
{
	const struct ship ship = g->ship;
	const column* const c = game_get_ship_column(g);
	const cell position = column_get_cell(c, (size_t) ship.ship_pos.y);
//	column* const c = game_get_ship_column(g);

	if(position == CELL_WALL){
		if(g->ship.life > 0){
			column_set_cell((column*) c, (size_t) ship.ship_pos.y, CELL_EMPTY);
			wallColision((game *) g);
		}
	}
	return true;
}

bool game_ship_is_alive(const game* const g)
{
	return g->ship.life > 0;
}
short game_get_life( const game* g){
	return (g->ship.life > 0)?g->ship.life: 0;
}
void game_set_life(  game* const g, short  life){
	g->ship.life = life;
}
void game_sub_life(  game* const g, short life){

	g->ship.life -= life;
}
double game_get_bullet_cooldown_start(const game* g)
{
	return g->cooldown_start;
}

double game_get_bullet_cooldown_threshold(const game* g)
{
	return g->cooldown_threshold;
}

enum gun game_get_gun_type(const game* g){
	return g->ship.gun;
}

short game_get_max_life(const game* g){
	return g->max_life;
}


enum gun game_get_gun(const game* game){
	return game->ship.gun;
}

double game_get_cooldown_treshold( const game* const game){
	return game->cooldown_threshold;
}


struct ship game_get_ship(const game* game){
	return game->ship;
}

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

void game_update(game* const g, const double timestamp, const int key)
{
	const spaceship_options options = g->options;

	if (key)
		game_process_input(g, key);

	/* Things that should always be checked. */
	game_check_cooldowns(g);

	if (game_isFireing(g)){
		if( g->elapsed_time - g->fire_time >  0.18 ){
			game_set_isFireing(g,false);

		}
	}

	if (game_isFireing(g))
		column_list_set_row_empty(
				terrain_get_columns(g->map),
				game_get_ship_position(g));

	if ((options.still && key == 's')
	    || (!options.still && game_turn_expected(g, timestamp)))
		game_compute_turn(g, timestamp);
}

bool game_turn_expected(game* g, double timestamp)
{
	/* Check whether a "full" turn must be computed. */
	const spaceship_options options = g->options;
	const double constant_delay = options.constant_delay;
	const double since_last_turn = timestamp - g->last_turn_timestamp;
	const double delay =
			constant_delay > 0.0 ? constant_delay : _threshold(options, g->elapsed_time);
	g->delay = delay;
	return since_last_turn > delay;
}

void game_compute_turn(game* const g, double timestamp)
{
	if (!game_turn_expected(g, timestamp))
		return;

	/* Compute the other steps. */
	game_check_special_cells(g);
	game_check_bullets(g);
	game_shift_right(g);
	game_check_bullets(g);
	game_move_bullets(g);
	game_check_bullets(g);
	game_fall(g);
	game_check_bullets(g);
	game_check_special_cells(g);

	/* Update the timestamp. */
	g->last_turn_timestamp = timestamp;
}

void game_set_cooldown_treshold( game* const game, float c){
	game->cooldown_threshold = c;
}

void game_set_start_timestamp(game* const g, const double t)
{
	g->start_timestamp = t;
}

void game_set_elapsed_time(game* const g, const double t)
{
	g->elapsed_time = t;
}

void game_set_bullet_cooldown_threshold(game* g, double threshold)
{
	g->cooldown_threshold = threshold;
}



void game_set_gun(game *const pGame, enum gun gun) {
	pGame->ship.gun = gun;
}


void game_set_max_ammo(game *const pGame, ushort i) {
	pGame->max_ammo = i;
}

void game_set_ammo(game *const pGame, ushort i) {
	pGame->bullet_ammo = i;
}
void game_set_toogle_isFireing(struct game* game){
	if (game_isFireing(game)){
		game_set_isFireing(game,false);
	}else{
		game_set_isFireing(game,true);
	}
}




void game_fire(game* g,intmax_t bullet_coefficient,size_t fired,ship ship){
	switch (g->ship.gun){
//				GUN_BLASTER impl
		case GUN_BLASTER:
		case GUN_ROCKET:
		case GUN_DIE_MOTHER_FUCKER_DIE:

			if (g->bullet_ammo > 0)
			{
				point p = (point) {
						.x = ship.ship_pos.x + 1,
						.y = ship.ship_pos.y ,
						.gun = game_get_gun(g)
				};
				if (!point_list_contains(g->bullets, p))
				{
					g->bullets = point_list_push_back(g->bullets, p);
					game_add_bonus(
							g, bullet_coefficient * (intmax_t) ((fired + 1) * (fired + 1)));
					g->bullet_ammo -= 1;
					g->cooldown_start = g->elapsed_time;
				}
			}
			break;

		case GUN_LASER:
			if (g->bullet_ammo > 0 && !game_isFireing(g))
			{
				game_set_isFireing(g,true);

				if(game_isFireing(g)){
					game_add_bonus(
							g, bullet_coefficient * (intmax_t) ((fired + 1) * (fired + 1)));
					column_list_set_row_empty(terrain_get_columns(g->map),game_get_ship_position(g));
					g->bullet_ammo -= 1;
					g->cooldown_start = g->elapsed_time;

				}


			}
			break;
//		case GUN_ROCKET:
////					todo: GUN_ROCKET gun impl
//			if (g->bullet_ammo > 0)
//			{
//				point p = (point) {
//						.x = ship.ship_pos.x + 1,
//						.y = ship.ship_pos.y ,
//						.gun = game_get_gun(g)};
//
//				if (!point_list_contains(g->bullets, p))
//				{
//					g->bullets = point_list_push_back(g->bullets, p);
//					game_add_bonus(
//							g, bullet_coefficient * (intmax_t) ((fired + 1) * (fired + 1)));
//					g->bullet_ammo -= 1;
//					g->cooldown_start = g->elapsed_time;
//				}
//			}
//			break;
//		case GUN_DIE_MOTHER_FUCKER_DIE:
//			if (g->bullet_ammo > 0) {
//				point p = (point) {
//						.x = ship.ship_pos.x + 1,
//						.y = ship.ship_pos.y,
//						.gun = game_get_gun(g)};
//
//				if (!point_list_contains(g->bullets, p)) {
//					g->bullets = point_list_push_back(g->bullets, p);
//					game_add_bonus(
//							g, bullet_coefficient * (intmax_t) ((fired + 1) * (fired + 1)));
//					g->bullet_ammo -= 1;
//					g->cooldown_start = g->elapsed_time;
//				}
//			}
//			break;
		default:
			break;
	}
}




void game_process_input(game* const g, const int key)
{
	const spaceship_options options = g->options;
	const int difficulty = options.difficulty;
	const intmax_t bullet_coefficient = options.bullet_coefficient;
	const size_t fired = point_list_get_size(g->bullets);

	terrain* const map = g->map;
	const int height = terrain_height(map);
	const int width = terrain_width(map);

	struct ship ship = g->ship;
	const size_t x = (size_t) ship.ship_pos.x;
	const size_t y = (size_t) ship.ship_pos.y;
	switch (key) {
		/* Haut. */
		case '8':
		case 'k':
		case 65:
			if (difficulty >= 2 && key != 'k')
				/* vim mode: move with h, j, k, l! */
				break;
			if (y > 0
			    && terrain_get_cell(map, x, y - 1) != CELL_WALL)
				ship.ship_pos.y--;
			break;
			/* Bas. */
		case '2':
		case 'j':
		case 66:
			/* vim mode: move with h, j, k, l! */
			if (difficulty >= 2 && key != 'j')
				break;
			if ((int) y < (height - 1)
			    && terrain_get_cell(map, x, y + 1) != CELL_WALL)
				ship.ship_pos.y++;
			break;
			/* Gauche. */
		case '4':
		case 'h':
		case 68:
			/* vim mode: move with h, j, k, l! */
			if (difficulty >= 2 && key != 'h')
				break;
			if (x >= 1 &&
			    terrain_get_cell(map, x - 1, y) != CELL_WALL)
				ship.ship_pos.x--;

			if (ship.ship_pos.x <= 0 && g->options.difficulty <= 0)
			{
				/* Si on a atteint le bord gauche. */
				terrain_left(map);
				ship.ship_pos.x++;
				point_list_shift_right(g->bullets);
			}
			break;
			/* Droite. */
		case '6':
		case 'l':
		case 67:
			/* vim mode: move with h, j, k, l! */
			if (difficulty >= 2 && key != 'l')
				break;
			if ((int) x < width
			    && terrain_get_cell(map, x + 1, y) != CELL_WALL)
				ship.ship_pos.x++;

			if (ship.ship_pos.x >= width - 1)
			{
				/* Si on a atteint le bord droit. */
				terrain_right(map);
				ship.ship_pos.x--;
				point_list_shift_left(g->bullets);
			}
			break;
//			###############################################################################
//			#########################                             #########################
//			#########################~~~~~~~~~~~FIRE~~~~~~~~~~~~~~#########################
//			#########################                             #########################
//			###############################################################################
			/* Space. */
		case ' ':
//			game_ship_set_isFireing(g,true);

			game_fire(g,bullet_coefficient,fired,ship);

			break;
		default:
			break;
	}

	g->map = map;
	g->ship = ship;
	g->last_key = key;
	game_check_special_cells(g);
}


////////////////////////////////////////////////////////////////////////////////
// local functions definitions
////////////////////////////////////////////////////////////////////////////////
void collision(game* g){
	const struct ship ship = g->ship;
	const column* const c = game_get_ship_column(g);
	const cell position = column_get_cell(c, (size_t) ship.ship_pos.y);
//	column* const c = game_get_ship_column(g);

	if(position == CELL_WALL){
		if(game_ship_is_alive(g)){
			column_set_cell((column*) c, (size_t) ship.ship_pos.y, CELL_EMPTY);
			wallColision((game *) g);
		}
	}
}

void game_add_bonus(game* const g, const intmax_t bonus)
{
	g->bonus += bonus;
}

intmax_t game_get_bonus(const game* const g)
{
	return g->bonus;
}

void game_move_bullets(game* const g)
{
	point_list_shift_right(g->bullets);
}

void game_shift_right(game* const g)
{
	terrain_right(g->map);
}

void game_check_special_cells(game* const g)
{
	const spaceship_options options = g->options;
	const struct ship ship = g->ship;
	column* const c = game_get_ship_column(g);

	cell position = column_get_cell(c, (size_t) ship.ship_pos.y);
	if (position == CELL_SECRET)
	{
		const int selector = (int) random() % 100;
		if (selector < 20)
			position = CELL_AMMO;
		else if (selector < 40)
			position = CELL_BONUS;
		else if (selector < 50)
			position = CELL_MALUS;
		else if (selector < 80)
			position = CELL_GUN;
		else
			position = CELL_EMPTY;
	}



	if (position == CELL_AMMO)
	{
		g->max_ammo += g->max_ammo < 10 ? 1 : 0;
		column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);
	}
	else if (position == CELL_BONUS)
	{
		game_add_bonus(g, options.bonus);
		if(game_get_life(g)+35 < game_get_max_life(g)){
			game_set_life(g,game_get_life(g)+35);
		}else{
			game_add_max_life(g,35);
		}
		column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);
	}
	else if (position == CELL_MALUS)
	{
		game_add_bonus(g, options.malus);
		column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);

	} else if (position == CELL_GUN){
		// if you your new gun will be same as previous, you will increase max ammo amount...

		game_set_isFireing(g,false);
		const int selector = (int) random() % 100;
		if(selector < 45){
			if(game_get_gun(g) != GUN_BLASTER){
				game_set_gun(g,GUN_BLASTER);
				game_set_ammo(g,4);
				game_set_max_ammo(g,4);
				game_set_cooldown_treshold(g,0.5);
			}else{
				g->max_ammo += g->max_ammo < 10 ? 1 : 0;
			}
			column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);

		}
		else if (selector < 65){
			if (game_get_gun(g) != GUN_LASER){
				game_set_gun(g,GUN_LASER);
				game_set_ammo(g,2);
				game_set_max_ammo(g,2);
				game_set_cooldown_treshold(g,2);
			} else{
				g->max_ammo += g->max_ammo < 10 ? 1 : 0;
			}

			column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);

		}
		else if (selector < 90){
			if (game_get_gun(g) != GUN_ROCKET){
				game_set_gun(g,GUN_ROCKET);
				game_set_ammo(g,3);
				game_set_max_ammo(g,3);
				game_set_cooldown_treshold(g,1);
			} else{
				g->max_ammo += g->max_ammo < 10 ? 1 : 0;
			}
			column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);

		}
		else{
			if(game_get_gun(g) != GUN_DIE_MOTHER_FUCKER_DIE){
				game_set_gun(g,GUN_DIE_MOTHER_FUCKER_DIE);
				game_set_ammo(g,1);
				game_set_max_ammo(g,1);
				game_set_cooldown_treshold(g,4);
			}else{
				g->max_ammo += g->max_ammo < 10 ? 1 : 0;
			}
			column_set_cell(c, (size_t) ship.ship_pos.y, CELL_EMPTY);

		}

	}
}

void game_add_max_life(game *const pGame, short i) {
	short newLife = game_get_max_life(pGame)+i;
	if(newLife > game_get_max_authorised_life(pGame)){
		game_set_max_life(pGame,200);
		game_set_life(pGame,200);
	}
	else{
		game_set_max_life(pGame,newLife);
		game_set_life(pGame,newLife);
	}
}

short game_get_max_authorised_life(game *const pGame) {
	return pGame->max_authorised_life;
}

void game_set_max_life(game *const pGame, short i) {
	pGame->max_life = i;
}


column* game_get_ship_column(const game* g)
{
	terrain* const map = g->map;
	const point ship = g->ship.ship_pos;
	const column_list* columns = terrain_get_columns(map);
	column* const c = column_list_get_column(columns, (size_t) ship.x);
	return c;
}

void game_fall(game* const g)
{
	terrain_fall(g->map);
}

void game_check_bullets(game* const g)
{
	const spaceship_options options = g->options;
	const point up_left = { .x = 0, .y = 0, };
	const point bottom_right = { .x = options.width, .y = options.height, };

	const column_list* map = terrain_get_columns(g->map);

	g->bullets = point_list_prune_out_of_bounds(g->bullets, up_left, bottom_right);
	const size_t count = point_list_get_size(g->bullets);
	for (size_t i = 0; i < count; ++i)
	{
		const point position = point_list_get_point(g->bullets, i);
		if (point_is_valid(position))
		{
			column* const c = column_list_get_column(map, (size_t) position.x);
			if (c && column_get_cell(c, (size_t) position.y) != CELL_EMPTY)
			{
				switch (position.gun){

					case GUN_BLASTER:
						column_set_cell(c, (size_t) position.y, CELL_EMPTY);
						point_list_set_point(g->bullets, i, point_invalid());
						break;
					case GUN_LASER:break;

					case GUN_ROCKET:

						column_list_set_cell_on_radius(
								terrain_get_columns(game_get_map(g)),
								position,CELL_EMPTY,5);
//						column_set_cell(c, (size_t) position.y, CELL_EMPTY);
						point_list_set_point(g->bullets, i, point_invalid());break;


					case GUN_DIE_MOTHER_FUCKER_DIE:
					// main bullet is working like rocket mechanics but it has also some random exposions on the map
					{
						column_list_set_cell_on_radius(
								terrain_get_columns(game_get_map(g)),
								position,CELL_EMPTY,4);

						for (ushort j = 0; j < (ushort) rand() % 8; ++j) {
							column_list_set_cell_on_radius(
									terrain_get_columns(game_get_map(g)),
									(point){
											.x =  (int) rand() % (terrain_width(game_get_map(g)) ),
											.y =  (int) rand() % (terrain_height(game_get_map(g)))},
									CELL_EMPTY,(ushort) rand() % 10);
						}
						point_list_set_point(g->bullets, i, point_invalid());
					}

						break;
				}

			}
		}
	}
	g->bullets = point_list_prune_out_of_bounds(g->bullets, up_left, bottom_right);

}

void game_check_cooldowns(game* const g)
{
	if (g->bullet_ammo >= g->max_ammo)
		return;

	const double elapsed_cooldown = g->elapsed_time - g->cooldown_start;
	if (elapsed_cooldown > g->cooldown_threshold)
	{
		g->bullet_ammo += 1;
		/* Set the cooldown for the next bullet. */
		g->cooldown_start = g->elapsed_time;
	}
}

double _threshold(const spaceship_options options, const double d)
{
	const int difficulty = options.difficulty;

	switch (difficulty)
	{
		case 0:
			return d > 59.0 ? 0.3 : (1.0 - d / 60.0);
			break;
		case 1:
			return d > 30.0 ? 0.25 : (1.0 - d / 45.0);
			break;
		default:
			return d > 14.0 ? 0.15 : (1.0 - d / 15.0);
			break;
	}
}

_Bool game_isFireing(struct game *game) {
	return game->isFireing;
}

void game_set_isFireing(struct game *game, bool b) {
	game->isFireing = b;
}





