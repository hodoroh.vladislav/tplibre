#include <stdio.h>

#define SPACESHIP_INFINITY_VERSION_MAJOR 0
#define SPACESHIP_INFINITY_VERSION_MINOR 0
#define SPACESHIP_INFINITY_VERSION_PATCH 0

static inline void print_version(void)
{
  printf(
      "spaceship-infinity %u.%u.%u\n",
      SPACESHIP_INFINITY_VERSION_MAJOR,
      SPACESHIP_INFINITY_VERSION_MINOR,
      SPACESHIP_INFINITY_VERSION_PATCH);
  printf("licensed under the WTFPLv2\n");
}


