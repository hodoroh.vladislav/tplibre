#ifndef _LISTE_H_
#define _LISTE_H_

/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#include <stddef.h>
#include <zconf.h>
#include "column.h"
#include "point.h"

////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////

struct column_list;
typedef struct column_list column_list;

struct columnBody;

typedef struct columnBody columnBody;
////////////////////////////////////////////////////////////////////////////////
// init./destroy etc.
////////////////////////////////////////////////////////////////////////////////

column_list* column_list_new(void);
void column_list_destroy(column_list* l);

////////////////////////////////////////////////////////////////////////////////q
// getters
////////////////////////////////////////////////////////////////////////////////

column* column_list_get_column(const column_list* l, size_t i);
size_t column_list_get_size(const column_list* l);

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

column_list* column_list_push_front(column_list* l, column* c);
column_list* column_list_push_back(column_list* l, column* c);
column_list* column_list_pop_front(column_list* l);
column_list* column_list_pop_back(column_list* l);
void column_list_set_row_empty(column_list*, point ship);
struct columnBody* column_list_get_from_index(struct column_list* columnList, size_t index);

void column_list_set_cell_on_radius(column_list* columnList, point position, enum cell cell,ushort radious);
#endif
