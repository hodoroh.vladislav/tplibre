#ifndef OPTIONS_H
#define OPTIONS_H

/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#include <stdio.h> 
#include <stdbool.h>
#include <inttypes.h>


////////////////////////////////////////////////////////////////////////////////
// macros
////////////////////////////////////////////////////////////////////////////////

#ifndef OPTIONS_DEFAULT_HEIGHT
  #define OPTIONS_DEFAULT_HEIGHT 18
#endif
#ifndef OPTIONS_DEFAULT_WIDTH
  #define OPTIONS_DEFAULT_WIDTH 80
#endif
#ifndef OPTIONS_DEFAULT_PRETTY
  #define OPTIONS_DEFAULT_PRETTY true
#endif
#ifndef OPTIONS_DEFAULT_DIFFICULTY
  #define OPTIONS_DEFAULT_DIFFICULTY 1
#endif
#ifndef OPTIONS_DEFAULT_BONUS
  #define OPTIONS_DEFAULT_BONUS 1000
#endif
#ifndef OPTIONS_DEFAULT_MALUS
  #define OPTIONS_DEFAULT_MALUS -1000
#endif
#ifndef OPTIONS_DEFAULT_BULLET_COEFFICIENT
  #define OPTIONS_DEFAULT_BULLET_COEFFICIENT -20
#endif
#ifndef OPTIONS_DEFAULT_BULLET_COOLDOWN
  #define OPTIONS_DEFAULT_BULLET_COOLDOWN .5
#endif
#ifndef OPTIONS_DEFAULT_HEALTH
  #define OPTIONS_DEFAULT_HEALTH 50
#endif

////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////

struct spaceship_options
{
  int height;
  int width;
  bool still;
  bool debug;
  bool help;
  bool version;
  bool invalid;
  int difficulty;
  double constant_delay;
  bool pretty;
  intmax_t bonus;
  intmax_t malus;
  int bullet_start_ammo;
  intmax_t bullet_coefficient;
  double bullet_cooldown;
};
typedef struct spaceship_options spaceship_options;

////////////////////////////////////////////////////////////////////////////////
// misc.
////////////////////////////////////////////////////////////////////////////////

void print_help(FILE* stream, const char* program);

////////////////////////////////////////////////////////////////////////////////
// init. / destroy etc.
////////////////////////////////////////////////////////////////////////////////

spaceship_options default_options(void);

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

void check_options(spaceship_options* o, int argc, char* argv[static argc + 1]);

#endif
