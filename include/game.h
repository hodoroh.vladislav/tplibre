#ifndef SPACESHIP_H
#define SPACESHIP_H

/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

#include "point.h"
#include "point_list.h"
#include "terrain.h"
#include "options.h"
#include "guntype.h"
////////////////////////////////////////////////////////////////////////////////
// types
////////////////////////////////////////////////////////////////////////////////



struct ship;
typedef struct ship ship;
struct game;
typedef struct game game;
struct ship;
typedef struct ship ship;



////////////////////////////////////////////////////////////////////////////////
// init./destroy etc.
////////////////////////////////////////////////////////////////////////////////

game* game_init(spaceship_options o);
void game_destroy(game* j);

////////////////////////////////////////////////////////////////////////////////
// getters
////////////////////////////////////////////////////////////////////////////////

spaceship_options game_get_options(const game* g);

/* Game status. */
bool game_ship_is_alive(const game* g);
int game_get_last_input(const game* g);
intmax_t game_get_score(const game* g);
double game_get_elapsed_time(const game* g);

double game_get_delay(const game* g);

/* Player ship. */
point game_get_ship_position(const game* g);

/* Map. */
terrain* game_get_map(const game* g);

/* Bullets. */
size_t game_get_bullet_ammo(const game* g);
size_t game_get_bullet_max_ammo(const game* g);
point_list* game_get_bullets(const game* g);
double game_get_bullet_cooldown_start(const game* g);
double game_get_bullet_cooldown_threshold(const game* g);

////////////////////////////////////////////////////////////////////////////////
// setters / modifiers
////////////////////////////////////////////////////////////////////////////////

void game_update(game* g, double timestamp, int key);
void game_set_start_timestamp(game* g, double t);
void game_set_elapsed_time(game* g, double t);

void game_check_cooldowns(game* g);
void game_compute_turn(game* g, double timestamp);
void game_process_input(game* g, int key);
void game_set_bullet_cooldown_threshold(game* g, double threshold);

void game_set_gun(game *const , enum gun );
enum gun game_get_gun(const game* );
void game_set_ammo(game *const pGame, ushort i);
void game_set_cooldown_treshold( game* const game, float c);
double game_get_cooldown_treshold(  const game* const);
short game_get_life( const game* g);
void game_sub_life(  game* const g,short life);
void game_set_life(  game* const g,short life);
void wallColision(game *pGame);
short game_get_max_life(const game*);
void collision(game* game);

void game_set_max_ammo(game *const pGame, ushort i);
void game_add_max_life(game *const pGame, short i);
void game_set_max_life(game *const pGame, short i);
short game_get_max_authorised_life(game *const pGame);

ship game_get_ship(const game* game);
void game_set_isFireing(struct game* game, bool b);
_Bool game_isFireing(struct game* game);
void game_set_fireTime(game* g,double current);

#endif
