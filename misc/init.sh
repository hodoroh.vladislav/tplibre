#!/usr/bin/env bash

dirs=(src include lib obj out bin misc)

case $1 in
    "create")  
        for i in "${dirs[@]}"; do
            mkdir "$i"
        done
        if [[ $2 = "-m" ]]; then
            mv *.c src
            mv *.h include
            mv *.o obj
            mv *.sh misc

        fi
    ;;
    "remove")
        for i in "${dirs}"; do
            rm -rf "$i"
        done
    ;;
esac









